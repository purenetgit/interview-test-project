﻿namespace SampleSite.Models.Entities
{
    public class User : BaseEntity
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public int Age { get; set; }
    }
}
